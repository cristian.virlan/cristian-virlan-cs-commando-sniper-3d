

Din pacate , pentru dimensiunea prea mare a intregului fisier pentru Unity (de peste 1 GB) , am incarcat pe gitlab doar folder-ul cu Assests-uri , iar pe Google Drive 
am incarcat intreg folder-ul pentru joc si care acesta trebuie bagat in Unity, nu cel de pe gitlab . Atasez link-ul pentru Google Drive :

https://drive.google.com/file/d/1gCeOfE8XYZUBqEfsBJhoGJuXjyTA5bhe/view?usp=sharing


Pasi pentru codul sursa : 
1. Instalarea Unity Hub
2. Instalarea Unity Engine ( !! doar versiunea 2019.4.39f1 , 64 bit !! )
3. In Unity Hub , click pe sagetica in joc de langa Open , apoi Add project from disk si selectati folder-ul Cs Commando sniper din arhiva pentru a-l incarca pe Unity Hub
4. Dupa incarcarea fisierului , apasati pe proiect pentru deschidere sau se v-a deschide automat dupa incarcare

Pasi pentru instalarea jocului (APK) : 
1. Instalarea unui emulator Android pentru Windows ( de preferat Gameloop , deooarece acesta l-am folosit si eu )
2. Dupa instalare , se v-a deschide o fereastra cu meniul principal al emulatorului , unde veti apasa pe cele 3 linii orizontale din dreapta sus
3. Apoi , apasati pe local APK installation
4. Selectati din calculator , APK-ul din arhiva 
5. Dupa instalare , jocul se va porni automat
